import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

class GraphTest {
    private Graph maze;

    @BeforeEach
    public void setUp(){
        maze = new Graph();
        maze.addVertex(3, Colour.BLUE);
        maze.addVertex(7, Colour.GREEN);
        maze.addEdge(maze.getVertexFromLabel(3),maze.getVertexFromLabel(7),Colour.PURPLE);
    }

    @org.junit.jupiter.api.Test
    void vertexTest() {
        assertEquals(maze.getVertexFromLabel(7).getColour(), Colour.GREEN);
        assertEquals(maze.getVertexFromLabel(3).getLabel(), 3);
    }

}