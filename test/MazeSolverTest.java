import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

class MazeSolverTest {
    private Graph maze;

    @BeforeEach
    public void setUp(){
        maze = new Graph();
    }

    @Test
    void solvePuzzleMaze() {
        maze.initVertices();
        maze.initEdges();

        int moveNumber = 0;
        State start = new State(maze.getVertexFromLabel(1),maze.getVertexFromLabel(2),moveNumber);
        moveNumber++;
        MazeSolver solver = new MazeSolver(maze);
        LinkedList<State> solutionStates = solver.solvePuzzleMaze(start,moveNumber);

        assertEquals(0, solutionStates.get(solutionStates.size()-1).getSecondPawnPosition().getLabel());
        assertEquals(2, solutionStates.get(0).getSecondPawnPosition().getLabel());
    }
}