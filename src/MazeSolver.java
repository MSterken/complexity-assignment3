import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MazeSolver {
    private final Graph maze;
    private final List<State> badStates;

    public MazeSolver(Graph maze) {
        this.maze = maze;
        badStates = new ArrayList<>();
    }



    /**
     *  In this method we check first if the state that is entered as an argument contains the finish position, if that is the case
     *      we return this state in a linked list. if not then we loop through all possible neighbours of the first pawn position and
     *      check if any are possible moves by comparing the colour of the position of the second pawn. if a move is possible this method
     *      is called again and it is passed the state with the possible move made. Because of the recursion if this call results in
     *      a legal path to the final position, this list will be returned. If no legal moves are possible the same thing is done for
     *      the neighbours of the second
     * @param start state where the methods starts checking neighbours off
     *
     * @param moveNumber indicates how many moves have come before this state was reached
     *
     * @return returns a linkedList of states, if not empty a solution has been found.
     */
    public LinkedList<State> solvePuzzleMaze(State start, int moveNumber){

        LinkedList<State> solution;
        if(start.finishReached()){
            solution = new LinkedList<>();
            solution.add(start);

            return solution;
        } else {
            for (Edge edge: maze.getEdges(start.getFirstPawnPosition())) {
                if (edge.getColour() == start.getSecondPawnPosition().getColour()){
                    State newState = new State(edge.getEnd(),start.getSecondPawnPosition(), moveNumber++);
                    if(!checkIfBadState(newState)){
                        solution = solvePuzzleMaze(newState, moveNumber++);
                        if(goalReached(solution)){
                            solution.addFirst(start);
                            return solution;
                        }
                    }
                }
            }
            for (Edge edge: maze.getEdges(start.getSecondPawnPosition())) {
                if (edge.getColour() == start.getFirstPawnPosition().getColour()){
                    State newState = new State(start.getFirstPawnPosition(), edge.getEnd(), moveNumber++);
                    if(!checkIfBadState(newState)){
                        solution = solvePuzzleMaze(newState, moveNumber++);
                        if(goalReached(solution)){
                            solution.addFirst(start);
                            return solution;
                        }
                    }
                }
            }
        }
        badStates.add(start);
        return new LinkedList<>();
    }

    /**
     * Checks for each state in the list if the goal has been reached.
     * @param solution the list of all visited states
     * @return returns true if one of the states has reached the finish vertex.
     */
    public boolean goalReached(LinkedList<State> solution){
        for (State state: solution) {
            if(state.finishReached()){
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if a state is effectively equal to a state previously visited which did not have any paths towards a solution
     * @param state the state which is being compared to previous "bad" states
     * @return returns true if the state being checked contains the exact 2 same positions as a previously visited bad state
     */
    public boolean checkIfBadState(State state){
        for (State badState: badStates) {
            if((state.getFirstPawnPosition() == badState.getFirstPawnPosition() && state.getSecondPawnPosition() == badState.getSecondPawnPosition())
            || (state.getSecondPawnPosition() == badState.getFirstPawnPosition() && state.getFirstPawnPosition() == badState.getSecondPawnPosition()) ){
                return true;
            }
        }
        return false;
    }
}
