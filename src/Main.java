import java.util.LinkedList;

public class Main {

    /**
     * initializes the maze, initializes the starting position of the two pawns and starts the count of moves made.
     * then initializes a Mazesolver class which is passed the maze and asks it to return a list of states that make up a
     * solution.
     * @param args
     */
    public static void main(String[] args) {
        Graph maze = new Graph();
        maze.initVertices();
        maze.initEdges();

        int moveNumber = 0;
        State start = new State(maze.getVertexFromLabel(1),maze.getVertexFromLabel(2),moveNumber);
        moveNumber++;
        MazeSolver solver = new MazeSolver(maze);
        LinkedList<State> solutionStates = solver.solvePuzzleMaze(start,moveNumber);

        System.out.println("Solution:");
        for (State state: solutionStates) {
            System.out.println(state);
        }

    }

}