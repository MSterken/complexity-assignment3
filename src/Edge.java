public class Edge {
    private final Colour colour;
    private final Vertex end;

    public Edge(Colour colour, Vertex end) {
        this.colour = colour;
        this.end = end;
    }

    public Colour getColour() {
        return colour;
    }

    public Vertex getEnd() {
        return end;
    }
}
