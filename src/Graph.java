import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Graph {
    private Map<Vertex, List<Edge>> verticesWithEdges;

    public Graph() {
        verticesWithEdges = new HashMap<>();
    }

    public void addVertex(int label, Colour colour) {
        verticesWithEdges.putIfAbsent(new Vertex(label, colour), new ArrayList<>());
    }

    public void addEdge(Vertex begin, Vertex end, Colour colour) {
        Edge edge = new Edge(colour, end);
        verticesWithEdges.get(begin).add(edge);
    }

    /**
     *
     * @param label the unique number a vertex is identified by
     * @return returns either a vertex if there is one with the matching label or null
     */
    public Vertex getVertexFromLabel(int label){
        for (Map.Entry entry: verticesWithEdges.entrySet()) {
            Vertex key = (Vertex) entry.getKey();
            if(key.getLabel() == label){
                return key;
            }
        }
        return null;
    }

    /**
     * Initiale all vertices, the vertex with label 0 is the finishing position with the unique colour blue.
     */
    public void initVertices(){
        addVertex(1,Colour.PURPLE);
        addVertex(2,Colour.BLACK);
        addVertex(3,Colour.GREEN);
        addVertex(4,Colour.GREEN);
        addVertex(5,Colour.GREEN);
        addVertex(6,Colour.ORANGE);
        addVertex(7,Colour.ORANGE);
        addVertex(8,Colour.PURPLE);
        addVertex(9,Colour.PURPLE);
        addVertex(10,Colour.BLACK);
        addVertex(11,Colour.ORANGE);
        addVertex(12,Colour.PURPLE);
        addVertex(13,Colour.ORANGE);
        addVertex(14,Colour.GREEN);
        addVertex(15,Colour.ORANGE);
        addVertex(16,Colour.GREEN);
        addVertex(17,Colour.GREEN);
        addVertex(18,Colour.BLACK);
        addVertex(19,Colour.ORANGE);
        addVertex(20,Colour.GREEN);
        addVertex(21,Colour.BLACK);
        addVertex(22,Colour.BLACK);
        addVertex(0,Colour.BLUE);
    }

    /**
     * Initialize all the edges
     */
    public void initEdges(){
        addEdge(getVertexFromLabel(1),getVertexFromLabel(4),Colour.PURPLE);
        addEdge(getVertexFromLabel(1),getVertexFromLabel(5),Colour.BLACK);
        addEdge(getVertexFromLabel(2),getVertexFromLabel(6),Colour.GREEN);
        addEdge(getVertexFromLabel(2),getVertexFromLabel(12),Colour.PURPLE);
        addEdge(getVertexFromLabel(3),getVertexFromLabel(1),Colour.ORANGE);
        addEdge(getVertexFromLabel(3),getVertexFromLabel(4),Colour.ORANGE);
        addEdge(getVertexFromLabel(4),getVertexFromLabel(13),Colour.BLACK);
        addEdge(getVertexFromLabel(5),getVertexFromLabel(9),Colour.ORANGE);
        addEdge(getVertexFromLabel(6),getVertexFromLabel(9),Colour.GREEN);
        addEdge(getVertexFromLabel(6),getVertexFromLabel(10),Colour.PURPLE);
        addEdge(getVertexFromLabel(7),getVertexFromLabel(2),Colour.GREEN);
        addEdge(getVertexFromLabel(8),getVertexFromLabel(3),Colour.PURPLE);
        addEdge(getVertexFromLabel(9),getVertexFromLabel(4),Colour.GREEN);
        addEdge(getVertexFromLabel(9),getVertexFromLabel(14),Colour.BLACK);
        addEdge(getVertexFromLabel(10),getVertexFromLabel(15),Colour.GREEN);
        addEdge(getVertexFromLabel(11),getVertexFromLabel(10),Colour.PURPLE);
        addEdge(getVertexFromLabel(11),getVertexFromLabel(12),Colour.GREEN);
        addEdge(getVertexFromLabel(12),getVertexFromLabel(7),Colour.GREEN);
        addEdge(getVertexFromLabel(13),getVertexFromLabel(8),Colour.GREEN);
        addEdge(getVertexFromLabel(13),getVertexFromLabel(18),Colour.GREEN);
        addEdge(getVertexFromLabel(14),getVertexFromLabel(20),Colour.ORANGE);
        addEdge(getVertexFromLabel(14),getVertexFromLabel(0),Colour.GREEN);
        addEdge(getVertexFromLabel(15),getVertexFromLabel(0),Colour.PURPLE);
        addEdge(getVertexFromLabel(15),getVertexFromLabel(22),Colour.GREEN);
        addEdge(getVertexFromLabel(16),getVertexFromLabel(15),Colour.GREEN);
        addEdge(getVertexFromLabel(17),getVertexFromLabel(16),Colour.BLACK);
        addEdge(getVertexFromLabel(17),getVertexFromLabel(11),Colour.BLACK);
        addEdge(getVertexFromLabel(17),getVertexFromLabel(12),Colour.PURPLE);
        addEdge(getVertexFromLabel(18),getVertexFromLabel(9),Colour.ORANGE);
        addEdge(getVertexFromLabel(18),getVertexFromLabel(20),Colour.ORANGE);
        addEdge(getVertexFromLabel(19),getVertexFromLabel(18),Colour.GREEN);
        addEdge(getVertexFromLabel(20),getVertexFromLabel(19),Colour.BLACK);
        addEdge(getVertexFromLabel(20),getVertexFromLabel(21),Colour.ORANGE);
        addEdge(getVertexFromLabel(21),getVertexFromLabel(0),Colour.BLACK);
        addEdge(getVertexFromLabel(21),getVertexFromLabel(22),Colour.ORANGE);
        addEdge(getVertexFromLabel(22),getVertexFromLabel(17),Colour.ORANGE);
    }

    public List<Edge> getEdges(Vertex position){
         return verticesWithEdges.get(position);
    }
}
