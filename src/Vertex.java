public class Vertex {
    private final int label;
    private final Colour colour;

    Vertex(int label, Colour colour) {
        this.label = label;
        this.colour = colour;
    }

    public int getLabel() {
        return label;
    }

    public Colour getColour() {
        return colour;
    }

    /**
     * The vertex that is the winning position is the only one with the colour blue.
     * @return returns true if the colour of the vertex is blue
     */
    public boolean isFinish(){
        return colour == Colour.BLUE;
    }

    @Override
    public String toString() {
        return "( " + label + " )";
    }
}
