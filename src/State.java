
public class State {
    private final Vertex firstPawnPosition;
    private final Vertex SecondPawnPosition;
    private final int moveNumber;

    public State(Vertex firstPawnPosition, Vertex secondPawnPosition, int moveNumber) {
        this.firstPawnPosition = firstPawnPosition;
        SecondPawnPosition = secondPawnPosition;
        this.moveNumber = moveNumber;
    }

    @Override
    public String toString() {
        return "Move " + moveNumber + ": P1" + firstPawnPosition + ", P2" + SecondPawnPosition;
    }

    public boolean finishReached(){
        return firstPawnPosition.isFinish() || SecondPawnPosition.isFinish();
    }

    public Vertex getFirstPawnPosition() {
        return firstPawnPosition;
    }

    public Vertex getSecondPawnPosition() {
        return SecondPawnPosition;
    }
}




